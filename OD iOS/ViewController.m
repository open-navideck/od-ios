//
//  ViewController.m
//  OD iOS
//
//  Created by Fotis Dimanidis on 29/11/14.
//  Copyright (c) 2014 navideck. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (weak, nonatomic) IBOutlet UITextField *titleLabel;
@property (weak, nonatomic) IBOutlet UITextField *addressLabel;
@property (weak, nonatomic) IBOutlet UITextField *categoryLabel;
@property (weak, nonatomic) IBOutlet UITextField *phoneLabel;
@property (strong,nonatomic) DIOSSession *session;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (strong,nonatomic) NSString *wktLocation;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (strong,nonatomic) NSString *nid;
@property (nonatomic,retain) UIActivityIndicatorView *activityView;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [self.locationManager startUpdatingLocation];
    
    _session = [DIOSSession sharedSession];
    NSString *sessionName = [[NSUserDefaults standardUserDefaults] objectForKey:@"cookiename"];
    NSString *sessionValue = [[NSUserDefaults standardUserDefaults] objectForKey:@"cookievalue"];
    NSString *tokenValue = [[NSUserDefaults standardUserDefaults] objectForKey:@"CSRF-Token"];
    
    if (!sessionName){
        [self login];
    }else{
        [_session addHeaderValue:[NSString stringWithFormat:@"%@=%@", sessionName, sessionValue] forKey:@"cookie"];
        [_session addHeaderValue:[NSString stringWithFormat:@"%@", tokenValue] forKey:@"X-CSRF-Token"];
    }
    
    //Fix image view
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected)];
    singleTap.numberOfTapsRequired = 1;
    _imageView.userInteractionEnabled = YES;
    [_imageView addGestureRecognizer:singleTap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)locate:(id)sender {
}

- (IBAction)submit:(id)sender {
}

- (IBAction)createNode:(id)sender {
    
    NSMutableDictionary *nodeData = [NSMutableDictionary new];
    
    //Set title
    [nodeData setObject:_titleLabel.text forKey:@"title"];
    
    //Set category
    NSDictionary *bodyValues = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:_categoryLabel.text, nil] forKeys:[NSArray arrayWithObjects:@"value", nil]];
    NSDictionary *languageDict = [NSDictionary dictionaryWithObject:[NSArray arrayWithObject:bodyValues] forKey:@"und"];
    [nodeData setObject:languageDict forKey:@"field_category"];
    
    //Set Phone
    bodyValues = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:_phoneLabel.text, nil] forKeys:[NSArray arrayWithObjects:@"value", nil]];
    languageDict = [NSDictionary dictionaryWithObject:[NSArray arrayWithObject:bodyValues] forKey:@"und"];
    [nodeData setObject:languageDict forKey:@"field_hotel_phone"];
    
    //Set Address
    bodyValues = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:_addressLabel.text, nil] forKeys:[NSArray arrayWithObjects:@"value", nil]];
    languageDict = [NSDictionary dictionaryWithObject:[NSArray arrayWithObject:bodyValues] forKey:@"und"];
    [nodeData setObject:languageDict forKey:@"field_hotel_address"];


    //Set location
    bodyValues = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"GEOFIELD_INPUT_WKT", _wktLocation, nil] forKeys:[NSArray arrayWithObjects:@"input_format", @"geom", nil]];
    languageDict = [NSDictionary dictionaryWithObject:[NSArray arrayWithObject:bodyValues] forKey:@"und"];
    [nodeData setObject:languageDict forKey:@"field_hotel_location"];
    
    //Set Node type
    [nodeData setObject:@"hotels" forKey:@"type"];  //New node need type
    
    
    _activityView=[[UIActivityIndicatorView alloc]     initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _activityView.center=self.view.center;
    [_activityView startAnimating];
    
    [self.view addSubview:_activityView];

    [DIOSNode nodeSave:nodeData success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //Successful node update
        _nid = [responseObject objectForKey:@"nid"];

        NSData *imgData = UIImageJPEGRepresentation(_imageView.image, 1.0);
        
        NSMutableDictionary *file = [[NSMutableDictionary alloc] init];
        NSString *base64Image = [(NSData *)imgData base64Encoding];
        [file setObject:base64Image forKey:@"file"];
        NSString *timestamp = [NSString stringWithFormat:@"%d", (int)[[NSDate date] timeIntervalSince1970]];
        NSString *imageTitle = _titleLabel.text;
        NSString *filePath = [NSString stringWithFormat:@"%@%@.jpg",@"public://app/", imageTitle];
        NSString *fileName = [NSString stringWithFormat:@"%@.jpg", imageTitle];
        [file setObject:filePath forKey:@"filepath"];
        [file setObject:fileName forKey:@"filename"];
        [file setObject:timestamp forKey:@"timestamp"];
        NSString *fileSize = [NSString stringWithFormat:@"%lu", (unsigned long)[imgData length]];
        [file setObject:fileSize forKey:@"filesize"];
        [DIOSFile fileSave:file success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSString *fid = [responseObject objectForKey:@"fid"];
            [file setObject:fid forKey:@"fid"];
            [file removeObjectForKey:@"file"];
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            [dict setObject:[NSString stringWithFormat:@"%@", fid] forKey:@"fid"];
            NSDictionary *fidLangDict = [NSDictionary dictionaryWithObject:[NSArray arrayWithObject:dict] forKey:@"und"];

            NSMutableDictionary *newNodeData = [NSMutableDictionary new];
            [newNodeData setObject:fidLangDict forKey:@"field_hotel_image"];
            
            [newNodeData setObject:_nid forKey:@"nid"];//specific id here
            [DIOSNode nodeUpdate:newNodeData success:^(AFHTTPRequestOperation *operation, id responseObject) {
                //Successful node update
                NSLog(@"Node updated");
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success" message:@"Operation successful!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
                [_activityView stopAnimating];
                [alert show];
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                //WE failed to update the node
                NSLog(@"Node not updated");
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil,nil];
                [_activityView stopAnimating];
                [alert show];
            }];
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Something went wrong: %@", [error localizedDescription]);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil,nil];
            [_activityView stopAnimating];
            [alert show];
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //Failed to update the node
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil,nil];
        [_activityView stopAnimating];
        [alert show];
    }];
}

- (void)login{
    NSLog(@"Logging in");
    [DIOSUser
     userLoginWithUsername:@"fdim_63"
     andPassword:@"swyXg8us6d67"
     success:^(AFHTTPRequestOperation *op, id response) {
         /* Handle successful operation here */
         NSLog(@"Logged in successfuly");
         [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"session_name"] forKey:@"cookiename"];
         [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"sessid"] forKey:@"cookievalue"];
         [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"token"] forKey:@"CSRF-Token"];
     }
     failure:^(AFHTTPRequestOperation *op, NSError *err) {
         /* Handle operation failire here */
         NSLog(@"%@",[err localizedDescription]);
         [_activityView stopAnimating];
     }
     ];
}

#pragma mark - UI code
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    //hide keyboard of autocomplete textfields when hitting "Done" on keyboard
    [textField resignFirstResponder];
    return NO;
}

#pragma mark - Location Manager Delegate Methods
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
//    NSLog(@"%@", [locations lastObject]);
    _locationLabel.text = [[NSString alloc]initWithFormat:@"lat: %.2f long: %.2f",[[locations lastObject] coordinate].latitude,[[locations lastObject] coordinate].longitude];
    _wktLocation = [[NSString alloc]initWithFormat:@"POINT (%f %f)",[[locations lastObject] coordinate].longitude,[[locations lastObject] coordinate].latitude];
}


#pragma mark - Image capture
-(void)tapDetected{
    NSLog(@"single Tap on imageview");
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    self.imageView.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

@end
