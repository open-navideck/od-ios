//
//  ViewController.h
//  OD iOS
//
//  Created by Fotis Dimanidis on 29/11/14.
//  Copyright (c) 2014 navideck. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DIOSNode.h"
#import "DIOSUser.h"
#import "DIOSSession.h"
#import <CoreLocation/CoreLocation.h>
#import "DIOSFile.h"

@interface ViewController : UIViewController <CLLocationManagerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>


@end

