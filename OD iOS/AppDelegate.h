//
//  AppDelegate.h
//  OD iOS
//
//  Created by Fotis Dimanidis on 29/11/14.
//  Copyright (c) 2014 navideck. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DIOSSession.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

